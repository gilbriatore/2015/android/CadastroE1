package br.edu.up.cadastroe1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class CadastroActivity extends AppCompatActivity {

    Pessoa pessoa;
    EditText txtNome;
    EditText txtIdade;
    EditText txtEstado;
    EditText txtReligiao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        txtNome = (EditText) findViewById(R.id.txtNome);
        txtIdade = (EditText) findViewById(R.id.txtIdade);
        txtEstado = (EditText) findViewById(R.id.txtEstado);
        txtReligiao = (EditText) findViewById(R.id.txtReligiao);

        Intent intent = getIntent();
        pessoa = (Pessoa) intent.getSerializableExtra("pessoa");

        if (pessoa != null){
            txtNome.setText(pessoa.getNome());
            String idade = String.valueOf(pessoa.getIdade());
            txtIdade.setText(idade);
            txtEstado.setText(pessoa.getEstado());
            txtReligiao.setText(pessoa.getReligiao());
        }
    }

    public void onClickGravar(View v) {

        if (pessoa == null){
            pessoa = new Pessoa();
        }

        pessoa.setNome(txtNome.getText().toString());
        int idade = Integer.parseInt(txtIdade.getText().toString());
        pessoa.setIdade(idade);
        pessoa.setEstado(txtEstado.getText().toString());
        pessoa.setReligiao(txtReligiao.getText().toString());

        BancoDeDados db = new BancoDeDados(this);
        db.gravar(pessoa);

        finish();
    }
}
