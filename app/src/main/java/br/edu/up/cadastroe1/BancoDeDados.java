package br.edu.up.cadastroe1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class BancoDeDados extends SQLiteOpenHelper {


    public BancoDeDados(Context context) {
        super(context, "exemplo", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE pessoas ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `nome` TEXT, `estado` TEXT, `religiao` TEXT, `idade` INTEGER )";
        db.execSQL(sql);
    }


    public void gravar(Pessoa pessoa){

        ContentValues valores = new ContentValues();
        valores.put("nome", pessoa.getNome());
        valores.put("idade", pessoa.getIdade());
        valores.put("estado", pessoa.getEstado());
        valores.put("religiao", pessoa.getReligiao());

        SQLiteDatabase db = getWritableDatabase();

        if (pessoa.getId() > 0){
            String id = String.valueOf(pessoa.getId());
            db.update("pessoas", valores, "id=?", new String[]{id});
        } else {
            db.insert("pessoas", null, valores);
        }
        db.close();
    }

    public List<Pessoa> listar(){

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("pessoas", null, null, null, null, null, null);

        List<Pessoa> lista = new ArrayList<>();

        while(cursor.moveToNext()){

            int idx0 = cursor.getColumnIndex("id");
            int idx1 = cursor.getColumnIndex("nome");
            int idx2 = cursor.getColumnIndex("idade");
            int idx3 = cursor.getColumnIndex("estado");
            int idx4 = cursor.getColumnIndex("religiao");

            Pessoa pessoa = new Pessoa();
            pessoa.setId(cursor.getInt(idx0));
            pessoa.setNome(cursor.getString(idx1));
            pessoa.setIdade(cursor.getInt(idx2));
            pessoa.setEstado(cursor.getString(idx3));
            pessoa.setReligiao(cursor.getString(idx4));

            lista.add(pessoa);
        }
        db.close();
        return lista;
    }

    public void excluir(Pessoa pessoa){

        String id = String.valueOf(pessoa.getId());

        SQLiteDatabase db = getWritableDatabase();
        db.delete("pessoas", "id=?", new String[]{id});
        db.close();
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Não é necessário programar ainda...
        //Somente quando for gerada nova versão do banco.
    }
}







